// Mackenzie Marshall
// V00152142

template <class T>
class Vector3{
public:
	// The default constructor to create a vector equal to the zero vector.
	Vector3(){
		x_ = 0;
		y_ = 0;
		z_ = 0;
	}

	// A constructor to create a vector with the specified x-, y-, and
	// z-components.
	Vector3(T x, T y, T z){
		x_ = x;
		y_ = y;
		z_ = z;
	}

	// The copy constructor to create a new vector that is equal to an
	// already existing vector.
	Vector3(const Vector3& v){
		x_ = v.x();
		y_ = v.y();
		z_ = v.z();
	}

	// Destroy a vector.
	~Vector3(){}

	// The assignment operator.
	// Assign, to an already existing vector, the value of another
	// already existing vector.
	Vector3& operator=(const Vector3& v){
		if(this != &v){
			x_ = v.x();
			y_ = v.y();
			z_ = v.z();
			return *this;
		}
		return *this;
	}

	// Get a reference to the x-component of the vector.
	const T& x() const{
		return x_;
	}
	T& x(){
		return x_;
	}

	// Get a reference to the y-component of the vector.
	const T& y() const{
		return y_;
	}
	T& y(){
		return y_;
	}

	// Get a reference to the z-component of the vector.
	const T& z() const{
		return z_;
	}
	T& z(){
		return z_;
	}

	// Get the squared norm (i.e., squared length) of the vector.
	// Note the word "squared" here.
	double sqrNorm() const{
		double result = x_*x_ + y_*y_ + z_*z_;
		return result;
	}

private:
	T x_, y_, z_;	
};

bool operator==(const Vector3<int>& v, const Vector3<int>& w){
	if(v.x()==w.x() && v.y()==w.y() && v.z()==w.z()){
		return true;
	}
	return false;
}

bool operator==(const Vector3<double>& v, const Vector3<double>& w){
	if(v.x()==w.x() && v.y()==w.y() && v.z()==w.z()){
		return true;
	}
	return false;
}

bool operator!=(const Vector3<int>& v, const Vector3<int>& w){
	if(v.x()==w.x() && v.y()==w.y() && v.z()==w.z()){
		return false;
	}
	return true;
}

bool operator!=(const Vector3<double>& v, const Vector3<double>& w){
	if(v.x()==w.x() && v.y()==w.y() && v.z()==w.z()){
		return false;
	}
	return true;
}

Vector3<int> operator*(const Vector3<int>& v, int a){
	Vector3<int> result;
	result.x() = v.x() * a;
	result.y() = v.y() * a;
	result.z() = v.z() * a;
	return result;
}

Vector3<double> operator*(const Vector3<double>& v, double a){
	Vector3<double> result;
	result.x() = v.x() * a;
	result.y() = v.y() * a;
	result.z() = v.z() * a;
	return result;
}

Vector3<int> operator*(int a, const Vector3<int>& v){
	Vector3<int> result;
	result.x() = v.x() * a;
	result.y() = v.y() * a;
	result.z() = v.z() * a;
	return result;
}

Vector3<double> operator*(double a, const Vector3<double>& v){
	Vector3<double> result;
	result.x() = v.x() * a;
	result.y() = v.y() * a;
	result.z() = v.z() * a;
	return result;
}

Vector3<int> operator+(const Vector3<int>& v, const Vector3<int>& w){
	Vector3<int> result;
	result.x() = v.x() + w.x();
	result.y() = v.y() + w.y();
	result.z() = v.z() + w.z();
	return result;
}

Vector3<double> operator+(const Vector3<double>& v, const Vector3<double>& w){
	Vector3<double> result;
	result.x() = v.x() + w.x();
	result.y() = v.y() + w.y();
	result.z() = v.z() + w.z();
	return result;
}

Vector3<int> operator-(const Vector3<int>& v, const Vector3<int>& w){
	Vector3<int> result;
	result.x() = v.x() - w.x();
	result.y() = v.y() - w.y();
	result.z() = v.z() - w.z();
	return result;
}

Vector3<double> operator-(const Vector3<double>& v, const Vector3<double>& w){
	Vector3<double> result;
	result.x() = v.x() - w.x();
	result.y() = v.y() - w.y();
	result.z() = v.z() - w.z();
	return result;
}

int dotProduct(const Vector3<int>& v, const Vector3<int>& w){
	int result;
	result = v.x()*w.x() + v.y()*w.y() + v.z()*w.z();
	return result;
}

double dotProduct(const Vector3<double>& v, const Vector3<double>& w){
	double result;
	result = v.x()*w.x() + v.y()*w.y() + v.z()*w.z();
	return result;
}

std::istream& operator>>(std::istream& inStream, Vector3<int>& v){
	inStream>>v.x();
	inStream>>v.y();
	inStream>>v.z();
	return inStream;
}

std::istream& operator>>(std::istream& inStream, Vector3<double>& v){
	inStream>>v.x();
	inStream>>v.y();
	inStream>>v.z();
	return inStream;
}

std::ostream& operator<<(std::ostream& outStream, const Vector3<int>& v){
	outStream<<v.x()<<" "<<v.y()<<" "<<v.z();
	return outStream;
}

std::ostream& operator<<(std::ostream& outStream, const Vector3<double>& v){
	outStream<<v.x()<<" "<<v.y()<<" "<<v.z();
	return outStream;
}
