// Mackenzie Marshall 
// V00152142

// This code can be used as a starting point for a testing program
// for the Vector3 class.

#include <cassert>
#include <iostream>
#include <sstream>
#include "Vector3.hpp"

template <class T> void testVector3(){
	Vector3<T> v0;
	assert(v0.x() == 0 && v0.y() == 0 && v0.z() == 0);
	Vector3<T> v1(1, 2, 3);
	assert(v1.x() == 1 && v1.y() == 2 && v1.z() == 3);
	v0 = v1;
	assert(v0.x() == 1 && v0.y() == 2 && v0.z() == 3);

	// Test input stream
	std::stringstream stream;
	stream.str("1  2     3");
	Vector3<T> v2;
	stream >> v2;
	assert(v2.x() == 1 && v2.y() == 2 && v2.z() == 3);
	
	// Test output stream.
	std::stringstream out;
	out << v2;
	assert(out.str() == "1 2 3");

	// Compairison tests	
	assert(v0 == v1);
	Vector3<T> v3;
	assert(v2 != v3);
	
	// multiplication and addition tests
	assert((v0+v1)==(v2*2));
	assert((v0+v1)==(2*v2));
	
	// sqr norm test
	assert(v1.sqrNorm() == 14);
	
	// subtraction test
	assert((v1-v0)==v3);
	
	// dot test
	assert(dotProduct(v1, v0)==14);
}

int main(int argc, char** argv)
{
	testVector3<int>();
	testVector3<double>();
	std::cout<<"successful\n";	
}