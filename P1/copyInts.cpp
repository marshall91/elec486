//Mackenzie Marshall
//V00152142
#include <iostream>
#include <cstdlib>
#include <string.h>
using namespace std;

void copyInts(int* source, int* destination, int size){
	memcpy(destination, source, sizeof(int)*size);
}

int main(){
	int source1[4] = {1, 2, 3, 4};
	int destination1[4];
	copyInts(source1, destination1, 4);
	cout<<"Test case 1:"<<endl;
	cout<<"Source: ";
	for(int i=0; i<4; i++){
		cout<<source1[i]<<" ";
	}
	cout<<endl;
	cout<<"Destination: ";
	for(int i=0; i<4; i++){
		cout<<destination1[i]<<" ";
	}
	cout<<endl;
	
	int source2[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	int destination2[10];
	copyInts(source2, destination2, 10);
	cout<<"Test case 2:"<<endl;
	cout<<"Source: ";
	for(int i=0; i<10; i++){
		cout<<source2[i]<<" ";
	}
	cout<<endl;
	cout<<"Destination: ";
	for(int i=0; i<10; i++){
		cout<<destination2[i]<<" ";
	}
	cout<<endl;
	
	int source3[6] = {1, 1, 1, 1, 1, 1};
	int destination3[6];
	copyInts(source3, destination3, 6);
	cout<<"Test case 3:"<<endl;
	cout<<"Source: ";
	for(int i=0; i<6; i++){
		cout<<source3[i]<<" ";
	}
	cout<<endl;
	cout<<"Destination: ";
	for(int i=0; i<6; i++){
		cout<<destination3[i]<<" ";
	}
	cout<<endl;
	return 0;
}