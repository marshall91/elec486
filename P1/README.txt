Mackenzie Marshall
V00152142

swap.cpp
Swap is not behaving correctly since the only the values not the reference of the variables are being passed. The new variables within the local functions are being swapped but not those in the main function. The simple correction is to make it a call by reference. Simply change:
void swap(int x, int y) to void swap(int& x, int& y)