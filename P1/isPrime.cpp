//Mackenzie Marshall
//V00152142
#include <iostream>
#include <cstdlib>
using namespace std;

bool isPrime(int x){
	for(int i=2; i<x; i++){
		if(x%i==0){
			return false;
		}
	}
	return true;
}

int main(){
	int x;
	while(cin>>x){
		if(x <= 1){
			exit(1);
		}
		bool is = isPrime(x);
		if(is == false){
			cout<<"composite"<<endl;
		}
		if(is == true){
			cout<<"prime"<<endl;
		}
	}
	return 0;
}
