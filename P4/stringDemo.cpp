// Mackenzie Marshall
// V00154142

#include <iostream>
#include <string>

int main(){
	std::string s;
	std::cin>>s;
	std::cout<<s.size()<<"\n";
	std::string r = std::string(s.rbegin(), s.rend());
	std::cout<<r<<"\n";
	std::cout<<s<<r<<"\n";
}