// Mackenzie Marshall
// V00152142

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

int main(){
	std::vector<double> values;
	double x;
	
	while(!std::cin.eof()){
		std::cin>>x;
		values.push_back(x);
	}
	if(values.size()==0) exit(1);
	std::vector<double>::iterator begin = values.begin();
	std::vector<double>::iterator end = values.end();
	std::vector<double>::const_iterator temp;
	for(std::vector<double>::const_iterator i = begin; i!=end; ++i){
		temp=i;
		if(++temp==end) std::cout<<*i<<"\n";
		else std::cout<<*i<<" ";
	}
	double max, min, mean, median;
	max = *std::max_element(begin, end);
	min = *std::min_element(begin, end);
	mean = std::accumulate(begin,end,0)/values.size();
	std::sort(begin, end);
	if(values.size()%2==1) median = values.at(values.size()/2);
	else{
		double temp1, temp2;
		temp1 = values.at(values.size()/2);
		temp2 = values.at(values.size()/2 - 1);
		median = (temp1+temp2)/2;
	}
	std::cout<<max<<" "<<min<<" "<<mean<<" "<<median<<"\n";
	for(std::vector<double>::const_iterator i = begin; i!=end; ++i){
		temp=i;
		if(++temp==end) std::cout<<*i<<"\n";
		else std::cout<<*i<<" ";
	}
}