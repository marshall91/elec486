Mackenzie Marshall
V00152142

Part 1:
First bug: begin and end iterator set before vector is filled. Issue is on line 9 and 10 with "std::vector<int>::iterator begin = values.begin();" and "std::vector<int>::iterator end = values.end();". This can be fixed be moving these statements after the while loop to line 18.

Second bug: Line 20, "std::vector<int>::iterator minValueIter = std::min_element(begin, end);" will not find minimum element as the begin and end iterators are not set correctly. This can be fixed by the same solution to the first bug by moving lines 9 and 10 to line 18.

Third bug: Line 27, "std::vector<int>::iterator newMinValueIter = std::min_element(begin, end);" may not necessarily find the minimum element. After pushing -1 to the end of the vector, end was not updated. this can be fixed by adding "std::vector<int>::iterator end = values.end();" to line 24.