// Mackenzie Marshall
// V00152142

#ifndef Complex_hpp
#define Complex_hpp

#include <iostream>

class Complex
{
public:
	//Defaul constructor
	Complex();
	
	//Standard constructor with real and imaginary components
	Complex(double r, double i);
	
	//Destructor
	~Complex();
	
	//Assignment Operator
	Complex& operator=(const Complex& n);
	
	//Get number of complex objects in in existence
	int getNumObjects() const;
	
	//Get a reference to the real component
	const double& r() const;
	double& r();
	
	//Get a reference to the imaginary component
	const double& i() const;
	double& i();
	
private:
	double r_, i_;
	static int total;
};

//Compute the sum of two complex numbers
Complex operator+(const Complex& a, const Complex& b);

//Compute the difference of two complex numbers
Complex operator-(const Complex& a, const Complex& b);

//Compute the multiplication of two complex numbers
Complex operator*(const Complex& a, const Complex& b);

//Compute the division of two complex numbers
Complex operator/(const Complex& a, const Complex& b);

//Output a complex number
std::ostream& operator<<(std::ostream& outStream, const Complex& n);

//Test two complex numbers for equality
bool operator==(const Complex& a, const Complex& b);

//Test two complex numbers for inequality
bool operator!=(const Complex& a, const Complex& b);

//Get magnitude of a complex number
double abs(const Complex& n);

//Get argument of a complex number
double arg(const Complex& n);

#endif