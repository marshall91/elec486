// Mackenzie Marshall
// V00152142

#ifndef DoubleVector3_hpp
#define DoubleVector3_hpp

#include <iostream>

class DoubleVector3
{
public:

	// The default constructor to create a vector equal to the zero vector.
	DoubleVector3();

	// A constructor to create a vector with the specified x-, y-, and
	// z-components.
	DoubleVector3(double x, double y, double z);

	// The copy constructor to create a new vector that is equal to an
	// already existing vector.
	DoubleVector3(const DoubleVector3& v);

	// Destroy a vector.
	~DoubleVector3();

	// The assignment operator.
	// Assign, to an already existing vector, the value of another
	// already existing vector.
	DoubleVector3& operator=(const DoubleVector3& v);

	// Get a reference to the x-component of the vector.
	const double& x() const;
	double& x();

	// Get a reference to the y-component of the vector.
	const double& y() const;
	double& y();

	// Get a reference to the z-component of the vector.
	const double& z() const;
	double& z();

	// Get the squared norm (i.e., squared length) of the vector.
	// Note the word "squared" here.
	double sqrNorm() const;
	
private:
	double x_, y_, z_;
};

// Test two vectors for equality.
bool operator==(const DoubleVector3& v, const DoubleVector3& w);

// Test two vectors for inequality.
bool operator!=(const DoubleVector3& v, const DoubleVector3& w);

// Compute the product of a vector and a scalar.
DoubleVector3 operator*(const DoubleVector3& v, double a);

// Compute the product of a scalar and a vector.
DoubleVector3 operator*(double a, const DoubleVector3& v);

// Compute the sum of two vectors.
DoubleVector3 operator+(const DoubleVector3& v, const DoubleVector3& w);

// Compute the difference of two vectors.
DoubleVector3 operator-(const DoubleVector3& v, const DoubleVector3& w);

// Compute the dot product of two vectors.
double dotProduct(const DoubleVector3& v, const DoubleVector3& w);

// Input a vector from a stream.
// The input data should be expected to be formatted as three
// real numbers separated by one or more whitespace characters.
std::istream& operator>>(std::istream& inStream, DoubleVector3& v);

// Output a vector to a stream.
// The output data should be formatted as three real numbers separated
// by exactly one space character, with no leading or trailing characters.
// For example, the vector (1,2,3) would be output to a stream as the
// sequence of characters "1 2 3", with no other additional whitespace
// characters.
std::ostream& operator<<(std::ostream& outStream, const DoubleVector3& v);

#endif
