// Mackenzie Marshall
// V00152142

#include "Complex.hpp"
#include <iostream>
#include <cmath>

int Complex::total=0;

Complex::Complex(){
	r_ = 0.0;
	i_ = 0.0;
	total++;
}

Complex::Complex(double r, double i){
	r_ = r;
	i_ = i;
	total++;
}

Complex::~Complex(){
	total--;
}

Complex& Complex::operator=(const Complex& n){
	if(this != &n){
		i_ = n.i();
		r_ = n.r();
		return *this;
	}
	return *this;
}

int Complex::getNumObjects() const{
	return total;
}

const double& Complex::r() const{
	return r_;
}

double& Complex::r(){
	return r_;
}

const double& Complex::i() const{
	return i_;
}

double& Complex::i(){
	return i_;
}

Complex operator+(const Complex& a, const Complex& b){
	Complex result;
	result.r() = a.r() + b.r();
	result.i() = a.i() + b.i();
	return result;
}

Complex operator-(const Complex& a, const Complex& b){
	Complex result;
	result.r() = a.r() - b.r();
	result.i() = a.i() - b.i();
	return result;
}

Complex operator*(const Complex& a, const Complex& b){
	Complex result;
	result.r() = a.r() * b.r();
	result.i() = a.i() * b.i();
	return result;
}

Complex operator/(const Complex& a, const Complex& b){
	Complex result;
	result.r() = a.r() / b.r();
	result.i() = a.i() / b.i();
	return result;
}

std::ostream& operator<<(std::ostream& outStream, const Complex& n){
	outStream<<"("<<n.r()<<","<<n.i()<<")";
	return outStream;
}

bool operator==(const Complex& a, const Complex& b){
	if(a.r()==b.r() && a.i()==b.i()){
		return true;
	}
	return false;
}

bool operator!=(const Complex& a, const Complex& b){
	if(a.r()==b.r() && a.i()==b.i()){
		return false;
	}
	return true;
}

double abs(const Complex& n){
	return std::sqrt(n.r()*n.r() + n.i()*n.i());
}

double arg(const Complex& n){
	return std::atan2(n.i(), n.r());
}