// Mackenzie Marshall
// V00152412
#include "DoubleVector3.hpp"
#include <iostream>
#include <cmath>

DoubleVector3::DoubleVector3(){
	x_ = 0;
	y_ = 0;
	z_ = 0;
}

DoubleVector3::DoubleVector3(double x, double y, double z){
	x_ = x;
	y_ = y;
	z_ = z;
}

DoubleVector3::DoubleVector3(const DoubleVector3& v){
	x_ = v.x();
	y_ = v.y();
	z_ = v.z();
}

DoubleVector3::~DoubleVector3(){}

DoubleVector3& DoubleVector3::operator=(const DoubleVector3& v){
	if(this != &v){
		x_ = v.x();
		y_ = v.y();
		z_ = v.z();
		return *this;
	}
	return *this;
}

const double& DoubleVector3::x() const{
	return x_;
}

double& DoubleVector3::x(){
	return x_;
}

const double& DoubleVector3::y() const{
	return y_;
}

double& DoubleVector3::y(){
	return y_;
}

const double& DoubleVector3::z() const{
	return z_;
}

double& DoubleVector3::z(){
	return z_;
}

double DoubleVector3::sqrNorm() const{
	double result = x_*x_ + y_*y_ + z_*z_;
	return result;
}

bool operator==(const DoubleVector3& v, const DoubleVector3& w){
	if(v.x()==w.x() && v.y()==w.y() && v.z()==w.z()){
		return true;
	}
	return false;
}

bool operator!=(const DoubleVector3& v, const DoubleVector3& w){
	if(v.x()==w.x() && v.y()==w.y() && v.z()==w.z()){
		return false;
	}
	return true;
}

DoubleVector3 operator*(const DoubleVector3& v, double a){
	DoubleVector3 result;
	result.x() = v.x() * a;
	result.y() = v.y() * a;
	result.z() = v.z() * a;
	return result;
}

DoubleVector3 operator*(double a, const DoubleVector3& v){
	DoubleVector3 result;
	result.x() = v.x() * a;
	result.y() = v.y() * a;
	result.z() = v.z() * a;
	return result;
}

DoubleVector3 operator+(const DoubleVector3& v, const DoubleVector3& w){
	DoubleVector3 result;
	result.x() = v.x() + w.x();
	result.y() = v.y() + w.y();
	result.z() = v.z() + w.z();
	return result;
}

DoubleVector3 operator-(const DoubleVector3& v, const DoubleVector3& w){
	DoubleVector3 result;
	result.x() = v.x() - w.x();
	result.y() = v.y() - w.y();
	result.z() = v.z() - w.z();
	return result;
}

double dotProduct(const DoubleVector3& v, const DoubleVector3& w){
	double result;
	result = v.x()*w.x() + v.y()*w.y() + v.z()*w.z();
	return result;
}

std::istream& operator>>(std::istream& inStream, DoubleVector3& v){
	inStream>>v.x();
	inStream>>v.y();
	inStream>>v.z();
	return inStream;
}

std::ostream& operator<<(std::ostream& outStream, const DoubleVector3& v){
	outStream<<v.x()<<" "<<v.y()<<" "<<v.z();
	return outStream;
}