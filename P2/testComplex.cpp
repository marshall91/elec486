// Mackenzie Marshall
// V00152142

// This code can be used as a starting point for a testing program
// for the Complex class.

#include <cassert>
#include <sstream>
#include <iostream>
#include "Complex.hpp"

int main(int argc, char** argv)
{
	// incomplete test
	Complex z(1.0, 2.0);
	assert(z.r() == 1.0 && z.i() == 2.0);
	
	std::cout<<"# of complex: "<<z.getNumObjects()<<"\n";
	
	Complex v(2.0, 2.0);
	std::cout<<"# of complex: "<<v.getNumObjects()<<"\n";
	
	z=v;
	std::cout<<"z=v: "<<z<<"\n";
	
	Complex x(3.0, 3.0);
	std::cout<<"Add test: "<<x+v<<"\n";
	std::cout<<"Sub test: "<<x-v<<"\n";
	std::cout<<"Mul test: "<<x*v<<"\n";
	std::cout<<"Div test: "<<x/v<<"\n";
	
	Complex cX(3.0, 3.0);
	if(x == cX){
		std::cout<<"x == copy of x\n";
	}
	if(v != cX){
		std::cout<<"v != copy of x\n";
	}
	
	std::cout<<"abs of x: "<<abs(x)<<"\n";
	std::cout<<"arg of x: "<<arg(x)<<"\n";
	
	std::cout << "successful" << std::endl;

	return 0;
}
