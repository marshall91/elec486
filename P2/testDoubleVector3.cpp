// Mackenzie Marshall
// V00152142

// This code can be used as a starting point for a testing program
// for the DoubleVector3 class.

#include <iostream>
#include <cassert>
#include "DoubleVector3.hpp"

int main(int argc, char** argv)
{
	// incomplete test
	DoubleVector3 v0;
	std::cout<<v0<<"\n";
	assert(v0.x() == 0 && v0.y() == 0 && v0.z() == 0);
	DoubleVector3 v1(1, 2, 3);
	std::cout<<v1<<"\n";
	assert(v1.x() == 1 && v1.y() == 2 && v1.z() == 3);
	std::cout<<"addition test\n";
	v0 = v1 + v1;
	std::cout << v0 << "\n";
	
	std::cout<<"in stream test:\n";
	DoubleVector3 v2;
	std::cin >> v2;
	std::cout << v2<<"\n";
	
	if(v0 == v2){
		std::cout<<"in stream == V0\n";
	}
	if(v1 != v2){
		std::cout<<"in stream != V1\n";
	}
	
	std::cout<<"* tests\n";
	std::cout<<v2*3<<"\n";
	
	std::cout<<4*v2<<"\n";
	
	std::cout<<"SqrdNorm of v1: "<<v1.sqrNorm()<<"\n";
	
	std::cout<<"v1-v0: "<<v1-v0<<"\n";
	
	std::cout<<"dot(v1,v0): "<<dotProduct(v1, v0)<<"\n";

	std::cout << "successful" << std::endl;

	return 0;
}
